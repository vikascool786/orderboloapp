import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SevaHomePage } from './seva-home';

@NgModule({
  declarations: [
    SevaHomePage,
  ],
  imports: [
    IonicPageModule.forChild(SevaHomePage),
  ],
})
export class SevaHomePageModule {}
