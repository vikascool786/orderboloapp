import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Form } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { Storage } from '@ionic/storage';
import { SevaOrderPage } from '../seva-order/seva-order';
import { SevaInAppPage } from '../seva-in-app/seva-in-app';
/**
 * Generated class for the SevaHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seva-home',
  templateUrl: 'seva-home.html',
})
export class SevaHomePage {
  Finaldata: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  imgCategoryUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/document_doctor/category/"
  categoryData: any;

   constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    public loadingCtrl: LoadingController, public generalService:GeneralProvider) {
      this.getCategory();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SevaHomePage');
  }

  //get All Categories
  getCategory() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"DC_categoryList";
    this.generalService.getCategoryList(url).subscribe(res=>{
      this.categoryData = res;
      this.Finaldata = this.categoryData.response.categories;
      // console.log(this.categoryData.response.categories);
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }

  goToDetailPage(id){
    // alert(id);
    this.navCtrl.push(SevaOrderPage, {
      data: id
    });
  }

  eSeva(){
    this.navCtrl.push(SevaInAppPage);
  }

}
