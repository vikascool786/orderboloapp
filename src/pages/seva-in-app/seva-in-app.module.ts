import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SevaInAppPage } from './seva-in-app';

@NgModule({
  declarations: [
    SevaInAppPage,
  ],
  imports: [
    IonicPageModule.forChild(SevaInAppPage),
  ],
})
export class SevaInAppPageModule {}
