import { NavController, NavParams,LoadingController, Form } from 'ionic-angular';
import { ProductListingPage } from '../product-listing/product-listing';
import { GeneralProvider } from '../../providers/general/general';

// Angular
import { Component, Injector, ViewChild } from '@angular/core';
// Ionic
import { IonicPage, Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-subcategory',
  templateUrl: 'subcategory.html',
})
export class SubcategoryPage {
  //for scrollable categories
  @ViewChild(Slides) slides: Slides;
  public selectedCategory: any;
  public categories: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;

  category_id: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  imgBaseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/";
  imgServiceUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/services_imgs/";
  imgProfileUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/";

  subCategoryData: any;
  Finaldata: any;
  subCategoryServicesList: any;
  subCategoryListDetails: any;
  allServices:boolean = true;
  subCategoryServices:boolean= false;
  subCategorySeriviceData: any;
  errorMsg:boolean = false;
  msg: string;
  customerReviewListByCategoryId: any;

  constructor(public injector: Injector, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController, public generalService:GeneralProvider) {
    this.category_id = navParams.get('data');
    this.getSubCategoryDetails(this.category_id);
    
  }

  //scrollable categories code start here--
  private initializeCategories(): void {

      // Select it by defaut
      this.selectedCategory = this.categories;

      // Check which arrows should be shown
      this.showLeftButton = false;
      this.showRightButton = this.categories.length > 3;
  }

  public filterData(categoryId: number): void {
      // alert(categoryId);
      this.getSubCategoryServiceDetails(categoryId);
      this.allServices = false;
      this.subCategoryServices = true;
  }

  // Method executed when the slides are changed
  public slideChanged(): void {
      let currentIndex = this.slides.getActiveIndex();
      this.showLeftButton = currentIndex !== 0;
      this.showRightButton = currentIndex !== Math.ceil(this.slides.length() / 3);
  }

  // Method that shows the next slide
  public slideNext(): void {
      this.slides.slideNext();
  }

  // Method that shows the previous slide
  public slidePrev(): void {
      this.slides.slidePrev();
  }
  //scrollable categories code end here--



  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcategoryPage');
  }

  //get all sub category in toolbar
  getSubCategoryDetails(id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"subCategoryListById";
    let formData = new FormData();
    formData.append('category_id', id);
    this.generalService.getSubCategoryListDetails(url, formData).subscribe(res=>{
      this.subCategoryData = res;
      if(this.subCategoryData.response.status = true){
        this.errorMsg = false;
        this.subCategoryListDetails = this.subCategoryData.response.subCategoryList;
        this.categories = this.subCategoryData.response.subCategoryList;
        this.subCategoryServicesList = this.subCategoryData.response.subCategoryServicesList;
        this.customerReviewListByCategoryId = this.subCategoryData.response.customerReviewListByCategoryId;
        console.log("test"+this.categories);
      }else{
        this.errorMsg = true;
      }
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }

  goToListingPage(id){
    this.navCtrl.push(ProductListingPage, {
      data: id
    });
  }

  //get subcategory Individual service by subcategory_id
  getSubCategoryServiceDetails(id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"getServiceListBySubCatId";
    let formData = new FormData();
    formData.append('subcategory_id', id);
    this.generalService.getServiceListBySubCatId(url, formData).subscribe(res=>{
      this.subCategorySeriviceData = res.response;
      console.log("services"+this.subCategorySeriviceData);
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }

  
}
