import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MakePaymentPage } from '../make-payment/make-payment';
import { UserProvider } from '../../providers/user/user';
import { GeneralProvider } from '../../providers/general/general';
import { Sim } from '@ionic-native/sim';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';



/**
 * Generated class for the SummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-summary',
  templateUrl: 'summary.html',
})
export class SummaryPage {
  data :any= [];
  photo: string;
  myPhoto: string;
  imgProfileUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/"
  phone: any;
  userData: any = {id:''};
  getDataUrl = {service_id:'', service_price:'', city:'', title:''};
  myDate:any;
  myTime:any;

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, private storage: Storage,public navParams: NavParams, private sim: Sim,  public UserProvider:UserProvider,public generalService:GeneralProvider) {
    this.storage.get('userDetails').then((val) => {
      this.data = JSON.parse(val);
      this.userData.id = this.data[0].id;
      if(this.photo == null || this.photo == '' || this.photo == undefined){
        // alert(this.photo);
      this.myPhoto = "http://www.qygjxz.com/data/out/42/4684590-cartoon-picture.png"; 
      }else{
        this.myPhoto = this.imgProfileUrl+this.photo;
      }
    });
    this.getDataUrl.service_id = navParams.get('service_id');
    this.getDataUrl.service_price = navParams.get('service_price');
    this.getDataUrl.title = navParams.get('title');
    this.getDataUrl.city = navParams.get('city');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SummaryPage');
  }

  showConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'We booked your booking...',
      message: 'Our Reprentative will contact you soon.',
      buttons: [
        {
          text: 'Thanks For Booking!',
          handler: () => {
            console.log('MakePaymentPage clicked');
            this.navCtrl.push(MakePaymentPage);
          }
        }
      ]
    });
    confirm.present();
  }

  getPayment(){
    alert(this.userData.id);
    if(this.userData.id == null || this.userData.id == '' || this.userData.id == undefined){
      this.navCtrl.push(LoginPage);
    }else{
      this.showConfirm(); 
    }
  }
}
