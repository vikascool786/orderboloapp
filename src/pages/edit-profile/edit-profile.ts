
import { Component, ViewChild, OnInit  } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { GeneralProvider } from '../../providers/general/general';
import { LoadingController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Sim } from '@ionic-native/sim';
import { TabsPage } from '../tabs/tabs';
import { OtpPage } from '../otp/otp';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { a } from '@angular/core/src/render3';

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  data: any;
  phone: any;
  address: any;
  created: any;
  showProfile:boolean = true;
  editProfile:boolean = false;
  userData = {id:'',phone:'', address:'',username:''}
  photo: any;
  myPhoto: string;
  fileName: string;
  imageURI: any;
  imageFileName: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  imgBaseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/"
  imgProfileUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/"
  userResponse: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sim: Sim,  public UserProvider:UserProvider,public generalService:GeneralProvider,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController, 
    private storage: Storage
    ) {
      this.storage.get('userDetails').then((val) => {
        this.data = JSON.parse(val);
        this.userData.id = this.data[0].id;
        this.userData.username = this.data[0].username;
        this.userData.phone = this.data[0].phone;
        this.userData.address = this.data[0].address;
        this.created = this.data[0].created;
        this.photo = this.data[0].photo;
        // console.log(this.data[0].phone);
        if(this.photo == null || this.photo == '' || this.photo == undefined){
          // alert(this.photo);
        this.myPhoto = "http://www.qygjxz.com/data/out/42/4684590-cartoon-picture.png"; 
        }else{
          this.myPhoto = this.imgProfileUrl+this.photo;
        }
      });
     
   
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  profilePhoto(){
    this.editProfile = true;
    this.showProfile = false;
  }

  updateProfile(){
    this.fileName = this.userData.phone+'_'+"profile.jpeg";
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture();
          }
        },
        {
          text: 'Load from Library',
          handler: () => {
            this.takeUploadPhoto();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }


  takePicture(){
    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.CAMERA
      // mediaType: this.camera.MediaType.PICTURE
    }
  
    this.camera.getPicture(options).then((imageData) => {
      // this.imageURI = 'data:image/jpeg;base64,' + imageData;
      this.imageURI = imageData;
      // alert(this.imageURI);
      this.uploadFile();      
    }, (err) => {
      console.log(JSON.stringify(err));
      // this.presentToast(err);
    });
  }

  takeUploadPhoto(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      // sourceType: this.camera.PictureSourceType.CAMERA
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.uploadFile();
    }, (err) => {
      console.log(JSON.stringify(err));
      // this.presentToast(err);
    });
  }
  

  uploadFile(){
    // localStorage.removeItem("userDetails");
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let createFileName = this.fileName;
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: createFileName,
      // fileName: 'ionicfile.jpeg',
      chunkedMode: false,
      // mimeType: "image/jpeg",
      headers: {},
      // mimeType: "multipart/form-data",
      params : {
        'phone': this.userData.phone,
        'address': this.userData.address,
        'username': this.userData.username,
        'photo': this.fileName
      }
    }
  
    fileTransfer.upload(this.imageURI, 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/updateProfile', options)
      .then((data) => {

        if(data.responseCode == 200){
          this.imageFileName = this.imageURI;
          loader.dismiss();
          this.presentToast("File uploaded successfully");
          this.navCtrl.setRoot(HomePage);
        }
      
    }, (err) => {
      // alert(JSON.stringify(err));
      loader.dismiss();
      this.presentToast(err);
    });
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }


  logout(){
    this.storage.clear();
    this.navCtrl.setRoot(HomePage);
  }

}
