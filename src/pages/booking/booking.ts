import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Form } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage implements OnInit{
  booking: string;
  after_login:boolean = false;
  before_login:boolean = true;
  data: any;
  userData = {user_id:'', username:'', phone:'', photo:'', created:'', address:''};
  id: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    public loadingCtrl: LoadingController, 
    public generalService:GeneralProvider) {
      this.booking = "current";
  }

  ionViewWillEnter(){
    this.booking = "current";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');
  }

  ngOnInit(){
    this.storage.get('userDetails').then((val) => {
      if(val){
        this.after_login = true;
        this.before_login = false;
      }else{
        this.after_login = false;
        this.before_login = true;
      }
    });
  }

  login(){
    this.navCtrl.push(LoginPage);
  }
}
