import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { GeneralProvider } from '../../providers/general/general';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';


/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  otp: any;
  phone: any;
  address: any;
  otpData = {otp:''}
  signupResponse: any;

  constructor(public navCtrl: NavController, private storage: Storage,
    public navParams: NavParams, public UserProvider:UserProvider,public generalService:GeneralProvider) {
    this.otp = navParams.get('otp');
    this.phone = navParams.get('phone');
    this.address = navParams.get('address');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  userActivate(){
    if(this.otp == this.otpData.otp){
      let userDatas = new FormData();
      userDatas.append('phone', this.phone);
      userDatas.append('address', this.address);
      userDatas.append('activation_code', this.otp);
      this.UserProvider.registerS(userDatas).subscribe(res=>{
        this.signupResponse = res.json();
        if(this.signupResponse.status == true){
          console.log("userDetails--->"+this.signupResponse.response.userDetails);
          this.storage.set('userDetails', JSON.stringify(this.signupResponse.response.userDetails));
          // localStorage.setItem('userDetails',JSON.stringify(this.signupResponse.response.userDetails)); 
          this.navCtrl.setRoot(HomePage);
        }
      });      
    }else{
      alert("Please Enter Valid OTP");
    }
  }



}
