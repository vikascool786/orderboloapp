
import { Component, ViewChild, OnInit  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { GeneralProvider } from '../../providers/general/general';
import { Sim } from '@ionic-native/sim';
import { TabsPage } from '../tabs/tabs';
import { OtpPage } from '../otp/otp';
import { LoginPage } from '../login/login';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { Storage } from '@ionic/storage';
import { AboutPage } from '../about/about';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit{
  data :any= [];
  showProfile:boolean = true;
  showFullProfile:boolean = false;
  address: any;
  phone: any;
  username: any;
  photo: any;
  myPhoto: string;
  userData = {user_id:'', username:'', phone:'', photo:'', created:'', address:''};
  userResponse: any;
  id: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  imgBaseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/"
  imgProfileUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/"

  constructor(public navCtrl: NavController, private storage: Storage,public navParams: NavParams, private sim: Sim,  public UserProvider:UserProvider,public generalService:GeneralProvider) {
    this.storage.get('userDetails').then((val) => {
      this.data = JSON.parse(val);
      if(JSON.parse(val)){
        this.photo = this.data[0].photo;
        // console.log(this.data[0].phone);
        if(this.photo == null || this.photo == '' || this.photo == undefined){
          // alert(this.photo);
        this.myPhoto = "http://www.qygjxz.com/data/out/42/4684590-cartoon-picture.png"; 
        }else{
          this.myPhoto = this.imgProfileUrl+this.photo;
        }
      }
    });
   
  }


  ngOnInit(){
    this.storage.get('userDetails').then((val) => {
      // console.log('Your age is', val);
      if(JSON.parse(val)){
        alert(val);
        this.data = JSON.parse(val);
        this.showProfile = false;
        this.showFullProfile = true;
        this.id = this.data[0].id
        this.userData.username = this.data[0].username;
        this.userData.phone = this.data[0].phone;
        this.userData.address = this.data[0].address;
        this.photo = this.imgProfileUrl+this.data[0].photo;
        if(this.data[0].username == '' || this.data[0].username == null||this.data[0].username == undefined){
          this.getUserFulldetails();
        }
      }else{
        this.showProfile = true;
        this.showFullProfile = false;
      }
    });    
  }

  getUserFulldetails(){
    let url = "http://vitsolutions24x7.com/orderbolo/api/api/LoginController/getUserDetailsById";
    let formData = new FormData()
    formData.append('user_id', this.id);
    this.generalService.getUserDetailsById(url, formData).subscribe(res=>{
      console.log(res);
      this.userResponse = res;
      if(this.userResponse.status == true){
        console.log("userDetails--->"+this.userResponse.response.userDetails);
        this.storage.set('userDetails',JSON.stringify(this.userResponse.response.userDetails)); 
      }
    })
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  
  login(){
    this.navCtrl.push(LoginPage)
  }

  editProfile(){
    this.navCtrl.push(EditProfilePage);
  }

  aboutUsPage(){
    this.navCtrl.push(AboutPage);
  }

}
