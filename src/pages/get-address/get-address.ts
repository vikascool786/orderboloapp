import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder,  NativeGeocoderReverseResult, NativeGeocoderForwardResult , NativeGeocoderOptions} from '@ionic-native/native-geocoder';
import { SummaryPage } from '../summary/summary';
/**
 * Generated class for the GetAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-get-address',
  templateUrl: 'get-address.html',
})
export class GetAddressPage {
  btnSubmit:boolean = true;
  vikasData: string;
  gotLocation:boolean = true;
  service_price: any;
  service_id: any;
  title: any;
  constructor(public navCtrl: NavController, private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder, public navParams: NavParams) {
    this.service_id = navParams.get('service_id');
    this.service_price = navParams.get('service_price');
    this.title = navParams.get('title');
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad GetAddressPage');
    
  
  }

  myMethod(data1, data2){
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(data1, data2, options)
    .then((result: NativeGeocoderReverseResult[]) => this.vikasData = JSON.stringify(result[0].locality))
    .catch((error: any) => console.log(error));
  }
  getCurrentLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      // alert(resp.coords.latitude); //18.520430299999997
      // alert(resp.coords.longitude); //73.8567437
      this.myMethod(resp.coords.latitude, resp.coords.longitude);
      this.vikasData = "Nagpur";
      this.gotLocation = false;
     
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

  getSummary(){
    // this.navCtrl.push(SummaryPage);
    this.navCtrl.push(SummaryPage, 
      {service_id: this.service_id, service_price:this.service_price, title:this.title, city:this.vikasData}
    );
  }

}
