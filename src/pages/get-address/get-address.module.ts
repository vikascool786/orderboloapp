import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetAddressPage } from './get-address';

@NgModule({
  declarations: [
    GetAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(GetAddressPage),
  ],
})
export class GetAddressPageModule {}
