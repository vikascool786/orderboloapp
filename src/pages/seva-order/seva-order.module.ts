import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SevaOrderPage } from './seva-order';

@NgModule({
  declarations: [
    SevaOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(SevaOrderPage),
  ],
})
export class SevaOrderPageModule {}
