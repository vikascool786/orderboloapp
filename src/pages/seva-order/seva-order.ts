import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, Form } from 'ionic-angular';
import { LoadingController, ActionSheetController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { DocumentProvider } from '../../providers/document/document'
import { GeneralProvider } from '../../providers/general/general';
import { Sim } from '@ionic-native/sim';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

/**
 * Generated class for the SevaOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seva-order',
  templateUrl: 'seva-order.html',
})
export class SevaOrderPage {
  id: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  documentResponse: any;
  MainResponse: any;
  fileName: string;
  imageURI: any;
  imageFileName: any;
  certificate_id: any;
  data: any;
  user_id: any;
  alreadyUploadedDoc: any;
  arrayPush = [];

  constructor(
    public navCtrl: NavController, public navParams: NavParams, private sim: Sim,public generalService:GeneralProvider,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController, 
    private storage: Storage,
    public DocumentProvider:DocumentProvider
    ) {
      this.storage.get('userDetails').then((val) => {
        if(val){
          // alert(val);
          this.data = JSON.parse(val);
          this.user_id = this.data[0].id;
          if(this.data[0].username == '' || this.data[0].username == null||this.data[0].username == undefined){
           this.storage.set('user_id', this.data[0].id); 
            // localStorage.removeItem("userDetails");
            this.id = this.data[0].id;
            this.storage.get('user_id').then((val) => {
              this.user_id = val;
              this.getAlreadyUploadDocByUser();
            });
          }
        }
        else{
          this.navCtrl.push(LoginPage);
        }
      });
      
       
      this.certificate_id = navParams.get('data');
      this.documentListByID();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SevaOrderPage');
  }

  ngOnInit(){
    // this.storage.get('userDetails').then((val) => {
    //   if(val){
    //     // alert(val);
    //     this.data = JSON.parse(val);
    //     this.user_id = this.data[0].id;
    //     if(this.data[0].username == '' || this.data[0].username == null||this.data[0].username == undefined){
    //      // this.storage.set('user_id', this.data[0].id); 
    //       // localStorage.removeItem("userDetails");
    //       this.id = this.data[0].id;
    //     }
    //   }else{
    //     this.navCtrl.push(LoginPage);
    //   }
    // });
    
    // this.storage.get('user_id').then((val) => {
    //   this.user_id = val;
    //   alert(this.user_id);
    // }); 
    // this.certificate_id = navParams.get('data');
    // this.documentListByID();
    // this.getAlreadyUploadDocByUser();
  }


  documentListByID(){
    let formData = new FormData();
    formData.append('id', this.certificate_id);
    let url = this.baseUrl+"DC_getCertificateListById";
    this.DocumentProvider.getCertificateListById(url, formData).subscribe(res=>{
      // console.log(res);
      this.documentResponse = res.response.documentList[0].documentList;
      this.MainResponse = res.response.documentList[0].mainDoc;
    })
  }

   
  getAlreadyUploadDocByUser(){
    let formData = new FormData();
    formData.append('user_id', this.id);
    let url = this.baseUrl+"DC_getDocUploadListByUserID";
    this.DocumentProvider.getDocListByUserID(url, formData).subscribe(res=>{
      this.alreadyUploadedDoc = res.response.documentList;
      // console.log(this.alreadyUploadedDoc);
      Object.keys(this.alreadyUploadedDoc).forEach(key => {
        let value = this.alreadyUploadedDoc[key];
        // console.log(value.certificate_name);
        this.arrayPush.push(value.certificate_id);
        console.log(this.arrayPush);
      });
    })
  }

  checkM(t){
    // console.log(this.arrayPush.indexOf(t));
    if(this.arrayPush.indexOf(t) !== -1){
      return true;
    }else{
      return false;
    }
  }


  upload_photo(certificate_id, name){
    this.certificate_id = certificate_id;
    this.fileName = name+".jpeg";
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture();
          }
        },
        {
          text: 'Load from Library',
          handler: () => {
            this.takeUploadPhoto();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }


  takePicture(){
    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.CAMERA
      // mediaType: this.camera.MediaType.PICTURE
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.uploadFile();      
    }, (err) => {
      // console.log(JSON.stringify(err));
    });
  }

  takeUploadPhoto(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      // sourceType: this.camera.PictureSourceType.CAMERA
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.uploadFile();
    }, (err) => {
      console.log(JSON.stringify(err));
      // this.presentToast(err);
    });
  }
  

  uploadFile(){
    // localStorage.removeItem("userDetails");
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let createFileName = this.fileName;
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: createFileName,
      // fileName: 'ionicfile.jpeg',
      chunkedMode: false,
      // mimeType: "image/jpeg",
      headers: {},
      // mimeType: "multipart/form-data",
      params : {
        'user_id': this.id,
        'certificate_id':this.certificate_id,
        'certificate_name': this.fileName
      }
    }
  
    fileTransfer.upload(this.imageURI, this.baseUrl+'DC_docUpload', options)
      .then((data) => {

        if(data.responseCode == 200){
          this.imageFileName = this.imageURI;
          loader.dismiss();
          this.presentToast("File uploaded successfully");
          this.getAlreadyUploadDocByUser();
        }
      
    }, (err) => {
      // alert(JSON.stringify(err));
      loader.dismiss();
      this.presentToast(err);
    });
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }


 
}
