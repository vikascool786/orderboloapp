import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Form } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the WalletPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet-payment',
  templateUrl: 'wallet-payment.html',
})
export class WalletPaymentPage {
  amount:any;
  data: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  userData: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    public generalService:GeneralProvider,
    public loadingCtrl: LoadingController,) {
      this.storage.get('userDetails').then((val) => {
        this.data = JSON.parse(val);
        this.userData.id = this.data[0].id;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPaymentPage');
  }

  addMoney(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"getServiceListBySubCatId";
    let formData = new FormData();
    formData.append('user_id', this.amount);
    formData.append('amout', this.userData.id);
    this.generalService.addAmountToWallet(url, formData).subscribe(res=>{
      console.log(res);
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }
}
