import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletPaymentPage } from './wallet-payment';

@NgModule({
  declarations: [
    WalletPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(WalletPaymentPage),
  ],
})
export class WalletPaymentPageModule {}
