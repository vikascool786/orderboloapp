import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SearchbarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searchbar',
  templateUrl: 'searchbar.html',
})
export class SearchbarPage {

  categoryItems:any = [
    'Beauty Parlor',
    'Garden Cleaning',
    'TV Repair Service',
    'Catering',
    'Home Cleaning'
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  initializeItems() {
    this.categoryItems = [
      'Beauty Parlor',
      'Garden Cleaning',
      'TV Repair Service',
      'Catering',
      'Home Cleaning'
    ];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.categoryItems = this.categoryItems.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchbarPage');
  }

}
