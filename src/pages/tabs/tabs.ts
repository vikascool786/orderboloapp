import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { WalletPage } from '../wallet/wallet';
import { BookingPage } from '../booking/booking';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = BookingPage;
  tab3Root = WalletPage;
  tab4Root = ProfilePage;

  constructor() {

  }
}
