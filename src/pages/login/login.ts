
import { Component, ViewChild, OnInit  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { GeneralProvider } from '../../providers/general/general';
import { Sim } from '@ionic-native/sim';
import { TabsPage } from '../tabs/tabs';
import { OtpPage } from '../otp/otp';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  simInfo: any;
  cards: any;
  test = this.simInfo;
  userData = {phone:'', address:'Nagpur',activation_code:''}
  signupResponse: any;
  activation_code:number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sim: Sim,  public UserProvider:UserProvider,public generalService:GeneralProvider) {
    // this.logged = localStorage.getItem("userLoggedIn");
  }

  ngOnInit(){
    // alert(this.logged);
    // if(this.logged == 'active' && this.user_id != 'undefined'){
    //   this.navCtrl.setRoot(DashboardPage);
    // }
  }


  async getSimData() {
    try {
      let simPermission = await this.sim.requestReadPermission();
      if (simPermission == "OK") {
        let simData = await this.sim.getSimInfo();
        this.simInfo = simData.cards.displayName;
        this.cards = simData.cards;
        alert(this.simInfo.displayName);
        console.log(simData);
        console.log(this.cards);
      }
    } catch (error) {
      console.log(error);
    }
  }
  
  signup(){
    let userDatas = new FormData();
    this.activation_code = Math.floor(1000 + Math.random() * 9000);
    console.log(this.activation_code);
    console.log(this.userData.phone);
    userDatas.append('phone', this.userData.phone);
    userDatas.append('address', this.userData.address);
    userDatas.append('activation_code', this.activation_code.toString());
    this.UserProvider.registerS(userDatas).subscribe(res=>{
      console.log(res.json());
      this.signupResponse = res.json();
      if(this.signupResponse.status == true){
          this.sendOtp();
          console.log("otp res"+this.signupResponse);
          this.navCtrl.push(OtpPage, {
            otp: this.activation_code,
            phone:this.userData.phone,
            address:this.userData.address
          });
      }else{
        localStorage.clear();
      }
    });
  }

  sendOtp(){
    this.generalService.getOtp(this.activation_code, this.userData.phone).subscribe(res=>{
      console.log(res)
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.getSimData();
  }

}
