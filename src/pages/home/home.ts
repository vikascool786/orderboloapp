import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Form } from 'ionic-angular';
import { SearchbarPage } from '../searchbar/searchbar';
import { SubcategoryPage } from '../subcategory/subcategory';
import { GeneralProvider } from '../../providers/general/general';
import { Storage } from '@ionic/storage';
import { SevaHomePage } from '../seva-home/seva-home';
import { ProductListingPage } from '../product-listing/product-listing';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  categoryData: any;
  Finaldata: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  imgBaseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/"
  imgServiceUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/services_imgs/"
  serviceData: any;
  featuerServices: any;
  data: any;
  userData = {user_id:'', username:'', phone:'', photo:'', created:'', address:''};
  id: string;
  userResponse: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,
    public loadingCtrl: LoadingController, public generalService:GeneralProvider) {
      this.getCategory();
      this.getFeatureServices();
  }

  ngOnInit(){
    this.storage.get('userDetails').then((val) => {
      if(val){
        // alert(val);
        this.data = JSON.parse(val);
        if(this.data[0].username == '' || this.data[0].username == null||this.data[0].username == undefined){
          this.userData = {user_id:'', username:'', phone:'', photo:'', created:'', address:''};
          localStorage.setItem('id', this.data[0].id);
          // localStorage.removeItem("userDetails");
          this.id = localStorage.getItem('id');
          this.getUserFulldetails();
        }
      }else{
        // alert("no storage");
      }
    });
    
  }

  getUserFulldetails(){
    let url = "http://vitsolutions24x7.com/orderbolo/api/api/LoginController/getUserDetailsById";
    let formData = new FormData()
    formData.append('user_id', this.id);
    this.generalService.getUserDetailsById(url, formData).subscribe(res=>{
      console.log(res);
      this.userResponse = res;
      if(this.userResponse.status == true){
        console.log("userDetails--->"+this.userResponse.response.userDetails);
        this.storage.set('userDetails',JSON.stringify(this.userResponse.response.userDetails)); 
      }
    })
  }
  gotToSearch(){
    this.navCtrl.push(SearchbarPage);
  }

  goToSubcategory(id){
    this.navCtrl.push(SubcategoryPage, {
      data: id
    });
  }

  getCategory() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"categoryList";
    this.generalService.getCategoryList(url).subscribe(res=>{
      this.categoryData = res;
      this.Finaldata = this.categoryData.response.categories;
      // console.log(this.categoryData.response.categories);
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }

  // go to product listing page
  goToListingPage(id){
    this.navCtrl.push(ProductListingPage, {
      data: id
    });
  }



  getFeatureServices() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"getServiceList";
    this.generalService.getFeatureServiceList(url).subscribe(res=>{
      // console.log(res);
      this.serviceData = res;
      this.featuerServices = this.serviceData.response.servicesData;
      console.log(this.serviceData.response);
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }

  //go to document doctor page
  goToDC(){
    this.navCtrl.push(SevaHomePage);
  }

}
