import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as sha512 from 'js-sha512';

/**
 * Generated class for the MakePaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-make-payment',
  templateUrl: 'make-payment.html',
})
export class MakePaymentPage {

  constructor(private iab: InAppBrowser, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakePaymentPage');
  }

  makePayment(){
    let name= "customer";
    let mobile= "9765924481";
    let email= "vikas@gmail.com";
    let bookingId= "testing";
    let productinfo= String(Math.floor(Math.random() * (99 - 10 +1) + 10) + String(1235));
    let salt= "1v30TxpzBV";
    let key= "3L5fuorg";
    let amt= "100";
    let surl= "https://www.payumoney.com/mobileapp/payumoney/success.php";
    let furl= "https://www.payumoney.com/mobileapp/payumoney/failure.php";
    let service_provider= "payu_paisa";
    let udf1= "";
    let udf2= "";
    let udf3= "";
    let udf4= "";
    let udf5= "";
    let udf6= "";
    let udf7= "";
    let udf8= "";
    let udf9= "";
    let udf10= "";

    let string = key + '|' + bookingId + '|' + amt + '|' + productinfo + '|' + name + '|' + email + '|' + udf1 + '|' + udf2 + '|' + udf3 + '|' + udf4 + '|' + udf5 + '|' +  udf6 + '|' +  udf7 + '|' +  udf8 + '|' +  udf9 + '|' +  udf10 + '|' + salt;
    let encrypttext = sha512.sha512(string);

    let url = "payumoney/payuBiz.html?amt="+ amt + "&service_provider="+ service_provider + "&name="+ name + "&surl="+ surl+"&furl="+furl+"&mobileNo="+mobile+"&email="+email+"&bookingId="+bookingId+"&productinfo="+productinfo+"&hash="+encrypttext+"&salt="+salt+"&key="+key;

    let options:InAppBrowserOptions = {
      location : 'yes',
      clearcache: 'yes',
      zoom: 'yes',
      toolbar: 'no',
      closebuttoncaption : 'back'
    };

    const browser = this.iab.create(url, '_blank', options);
    
    browser.on('loadstop').subscribe(event => {
      browser.executeScript({
        file: "payumoney/payumoneyPaymentGateway.js"
      });

      if(event.url = surl){
        browser.close();
      }
      if(event.url = furl){
        browser.close();
      }

    });
  }
}
