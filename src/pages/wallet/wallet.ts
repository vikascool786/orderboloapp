import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Form } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { WalletPaymentPage } from '../wallet-payment/wallet-payment';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage implements OnInit{
  after_login:boolean = false;
  before_login:boolean = true;
  data: any;
  userData = {user_id:'', username:'', phone:'', photo:'', created:'', address:''};
  id: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    public loadingCtrl: LoadingController, 
    public generalService:GeneralProvider) {
      this.storage.get('userDetails').then((val) => {
        if(val){
          this.after_login = true;
          this.before_login = false;
        }else{
          this.after_login = false;
          this.before_login = true;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');
  }

  ngOnInit(){
    this.storage.get('userDetails').then((val) => {
      if(val){
        this.after_login = true;
        this.before_login = false;
      }else{
        this.after_login = false;
        this.before_login = true;
      }
    });
    
  }


  login(){
    this.navCtrl.push(LoginPage);
  }
  
  goToAddMoneyPage(){
    this.navCtrl.push(WalletPaymentPage);
  }
}
