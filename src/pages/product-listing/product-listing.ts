import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController, Form } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { GetAddressPage } from '../get-address/get-address';

/**
 * Generated class for the ProductListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-listing',
  templateUrl: 'product-listing.html',
})
export class ProductListingPage {
  relationship:any;
  service_id: any;
  baseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/";
  imgBaseUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/";
  imgServiceUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/services_imgs/";
  imgProfileUrl:any = "http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/";
  serviceData: any = [];
  mydata: any;
  title:'';
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController, public generalService:GeneralProvider) {
    this.service_id = navParams.get('data');
    this.getServiceDetails(this.service_id);
    // alert(this.service_id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductListingPage');
  }

  getAddress(service_id, service_price, title){
    // alert(service_price);
    // alert(service_id);
    // this.navCtrl.push(GetAddressPage);
    this.navCtrl.push(GetAddressPage, 
      {service_id: service_id, service_price:service_price, title:title}
    );
  }
  
  getServiceDetails(service_id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let url = this.baseUrl+"getServiceByID";
    let formData = new FormData();
    formData.append('id', service_id);
    this.generalService.getServiceDetailsById(url, formData).subscribe(res=>{
      this.mydata = res.response.serviceData;
      console.log("services Details=>"+this.mydata);
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    })
  }

}
