import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  Url: string = 'http://vitsolutions24x7.com/orderbolo/api/api/';
  
  constructor(public http: Http) {
    console.log('Hello LoginProvider Provider');
  }

  //  login service
   public loginS(userData){
    let url: string = this.Url+'login.php';
    return this.http.post(url, userData)
    .map(res => res);
  }  

    //  register service
    public registerS(registerData){
      // alert(JSON.stringify(registerData));
      let url: string = this.Url+'signup';
      return this.http.post(url, registerData)
      .map(res => res);
    }  

    sendOtp(mobile, OTP){
      let otpUrl:string = "http://5.189.153.48:8080/vendorsms/pushsms.aspx?user=MAHA%20E%20SEVA&password=Pass@1234&msisdn="+mobile+"&sid=ORDERB&msg=Your%20OTP:"+OTP+"&fl=0&gwid=2";
      return this.http.get(otpUrl)
      .map(res => res.json());
    }
  
    //get users already uploaded document list
    public getUsersDocument(user_id){
      let url: string = this.Url+'check_uploaded_doc_list_by_id.php';
      return this.http.post(url, user_id)
      .map(res => res.json());
    }  
    
    // validate user otp
    public validateUser(otp){
      let url:string = this.Url+'validate.php';
      return this.http.post(url, otp)
      .map(res => res.json());
    }

  //document lists
  getDocumentLists() {
    return this.http.get(this.Url+'document_list.php').
      map(res => res.json());
  }

 //document lists
  getDocumentListById(id) {
    let params = {
        id: id
    };
    return this.http.get(this.Url+'document_child_list.php', {params}).
      map(res => res.json());
  }
  
}
