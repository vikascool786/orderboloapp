import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the DocumentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DocumentProvider {

 
  constructor(public http: Http) {
    console.log('Hello GeneralProvider Provider');
  }

  getOtp(otp, mobile){
    console.log(otp);
    return this.http.get("http://5.189.153.48:8080/vendorsms/pushsms.aspx?user=MAHA%20E%20SEVA&password=Pass@1234&msisdn="+mobile+"&sid=ORDERB&msg=Orderbolo%20sent%20you%20otp%20"+otp+"&fl=0&gwid=2").
    map(res => res.json());

  }

  getCertificateListById(url, data){
    console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }
  
  getDocListByUserID(url, data){
    console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }
  
}
