import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the GeneralProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeneralProvider {

  constructor(public http: Http) {
    console.log('Hello GeneralProvider Provider');
  }

  getCategoryList(url){
    return this.http.get(url).
    map(res => res.json());

  }

  getOtp(otp, mobile){
    console.log(otp);
    // return this.http.get("http://5.189.153.48:8080/vendorsms/pushsms.aspx?user=MAHA%20E%20SEVA&password=Pass@1234&msisdn=9765924481&sid=ORDERB&msg=test%20message&fl=0&gwid=2").
    return this.http.get("http://5.189.153.48:8080/vendorsms/pushsms.aspx?user=MAHA%20E%20SEVA&password=Pass@1234&msisdn="+mobile+"&sid=ORDERB&msg=Orderbolo%20sent%20you%20otp%20"+otp+"&fl=0&gwid=2").
    map(res => res.json());

  }

  getSubCategoryListDetails(url, data){
    return this.http.post(url, data).
    map(res => res.json());

  }

  getFeatureServiceList(url){
    return this.http.get(url).
    map(res => res.json());
  }

  getUserDetailsById(url, data){
    console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }
  

  getServiceListBySubCatId(url, data){
    console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }
  

  getServiceDetailsById(url, data){

    // console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }
  
  paytmPayment(url, data){
    console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }

  addAmountToWallet(url, data){
    console.log(url + data);
    return this.http.post(url, data).
    map(res=>res.json());
  }


}
