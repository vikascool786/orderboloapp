import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
//plugins
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Sim } from '@ionic-native/sim';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { IonicStorageModule } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
//pages
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { SearchbarPage } from '../pages/searchbar/searchbar';
import { SubcategoryPage } from '../pages/subcategory/subcategory';
import { ProductListingPage } from '../pages/product-listing/product-listing';
import { GetAddressPage } from '../pages/get-address/get-address';
import { MakePaymentPage } from '../pages/make-payment/make-payment';
import { SummaryPage } from '../pages/summary/summary';
import { OtpPage } from '../pages/otp/otp';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { WalletPage } from '../pages/wallet/wallet';
import { BookingPage } from '../pages/booking/booking';
import { SevaInAppPage } from '../pages/seva-in-app/seva-in-app';

//providers
import { UserProvider } from '../providers/user/user';
import { GeneralProvider } from '../providers/general/general';
import { SevaHomePage } from '../pages/seva-home/seva-home';
import { SevaOrderPage } from '../pages/seva-order/seva-order';
import { DocumentProvider } from '../providers/document/document';
import { WalletPaymentPage } from '../pages/wallet-payment/wallet-payment';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    ProfilePage,
    LoginPage,
    TabsPage,
    SearchbarPage,
    SubcategoryPage,
    ProductListingPage,
    GetAddressPage,
    MakePaymentPage,
    SummaryPage,
    OtpPage,
    EditProfilePage,
    WalletPage,
    BookingPage,
    SevaHomePage,
    SevaOrderPage,
    SevaInAppPage,
    WalletPaymentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    ProfilePage,
    LoginPage,
    TabsPage,
    SearchbarPage,
    SubcategoryPage,
    ProductListingPage,
    GetAddressPage,
    MakePaymentPage,
    SummaryPage,
    OtpPage,
    EditProfilePage,
    WalletPage,
    BookingPage,
    SevaHomePage,
    SevaOrderPage,
    SevaInAppPage,
    WalletPaymentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    NativeGeocoder,
    Sim,
    UserProvider,
    GeneralProvider,
    FileTransfer,
    // FileUploadOptions,
    FileTransferObject,
    File,
    Camera,
    DocumentProvider,
    InAppBrowser
  ]
})
export class AppModule {}
